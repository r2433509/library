Операция. makeNewRequest(clientID: integer, lCard_info: string, bookID: integer, title: string)

Ссылки: Прецеденты: Выдача книг(и)

Предусловия: Получен запрос на книгу(и)

Постусловия:
- Создан экземпляр seклассаSearchEngine (создание экземпляра)
- Атрибутамse.clientID, se.lCard_info, se.bookID, se.titleприсвоены соответствующие им переданные значения (модификация атрибута)
- Экземпляр seсвязан с классамиLCardDataBase и BooksDataBase на основе соответствия идентификаторов clientID и bookID, соответственно (формирование ассоциации)
